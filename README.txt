Context Region Title 1.x for Drupal 6.x
---------------------------------------
Context Region Title allows you to add a custom title for each region of your site.

It works with the magic of the Context module, which means you can choose custom region titles for different sections or even pages of your site.

See http://drupal.org/project/context for more information.


Installation
------------
Context can be installed like any other Drupal module -- place it in
the modules directory for your site and enable it on the 'admin/build/modules' page.


Usage
-----
By itself, the module simply provides variables that you can use in your theme.
If you're new to theming, see http://drupal.org/documentation/theme

The variables have the following naming convention:

$[region_name]_title

For example, the title for the region called 'left' will be called $left_title.


Example
-------
For example, the Garland theme in Drupal 6 outputs the right-sidebar region like this:

<?php if ($right): ?>
  <div id="sidebar-right" class="sidebar">
    <?php if (!$left && $search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>
    <?php print $right ?>
  </div>
<?php endif; ?>


We can add a line like this, which checks if a title has been set, and if so, prints it with a bit of markup around it:

<?php if ($right_title): ?><h2 class="title"><?php print $right_title ?></h2><?php endif; ?>


The end result could be something like this:

<?php if ($right): ?>
  <div id="sidebar-right" class="sidebar">
    <?php if ($right_title): ?><h2 class="title"><?php print $right_title ?></h2><?php endif; ?>
    <?php if (!$left && $search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>
    <?php print $right ?>
  </div>
<?php endif; ?>                