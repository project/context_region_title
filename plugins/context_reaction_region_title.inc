<?php

/**
 * Expose region titles as context reactions.
 */
class context_reaction_region_title extends context_reaction {
  /**
   * Editor form.
   */
  function editor_form($context) {
    $form = $this->options_form($context);
    return $form;
  }

  /**
   * Submit handler for editor form.
   */
  function editor_form_submit($context, $values) {
    return $values;
  }

  /**
   * Allow admins to provide region titles.
   */
  function options_form($context) {
    $theme_key = variable_get('theme_default', 'garland');
    $values = $this->fetch_from_context($context);
    $form = array(
      '#tree' => TRUE,
      '#title' => t('Region Titles'),
    );
    foreach (system_region_list($theme_key) as $region => $label) {
      $form[$region .'_title'] = array(
        '#title' => t('@label Title', array('@label' => $label)),
        '#description' => t('Provides this text as a <strong>$@title</strong> variable for display in page.tpl.php when this context is active.', array('@title' => $region .'_title')),
        '#type' => 'textfield',
        '#maxlength' => 255,
        '#default_value' => isset($values[$region .'_title']) ? $values[$region .'_title'] : '',
      );
      
    }
    return $form;
  }

  /**
   * Set the region titles.
   */
  function execute(&$vars) {
    $theme_key = variable_get('theme_default', 'garland');
    foreach ($this->get_contexts() as $k => $v) {
      foreach (system_region_list($theme_key) as $region => $label) {
        if (!empty($v->reactions[$this->plugin][$region.'_title']) && !isset($vars[$region.'_title'])) {
          $vars[$region.'_title'] = t($v->reactions[$this->plugin][$region.'_title']);
        }
      }
    }
  }
}
